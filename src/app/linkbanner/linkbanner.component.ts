import { Component, OnInit, Input } from '@angular/core';
import { Banner, BrokerData } from '../event/BrokerData';

@Component({
  selector: 'linkbanner',
  templateUrl: './linkbanner.component.html',
})
export class LinkbannerComponent implements OnInit {
  banner: Banner;

  @Input() className: string;
  @Input() broker: BrokerData;
  @Input() bannerType: string;

  ngOnInit(): void {
    try {
      this.banner = (this.broker != null && this.bannerType != null) ? this.broker.bannersMap[this.bannerType] : null;
    } catch (e) {
      this.banner = null;
    }
  }

}
