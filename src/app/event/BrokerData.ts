import { environment } from '../../environments/environment';

export class Banner {
  bannerType: string;
  redirectUrl: string;
  imageUrl: string;

  static copy(broker: BrokerData, data: Banner): Banner {
    let result = new Banner();
    result = Object.assign(result, data);
    result.imageUrl = `${environment.apiHost}/widget/image/${broker.brokerName}/${data.bannerType}`;
    return result;
  }
}

export class BrokerData {
  name: string;
  banners: Banner[];
  bannersMap: { [type: string]: Banner };
  get brokerName() { return this.name; }

  static copy(data: BrokerData): BrokerData {
    let result = new BrokerData();
    result = Object.assign(result, data);
    result.banners = Array.from(data.banners, banner => Banner.copy(result, banner));
    result.bannersMap = {};
    result.banners.forEach(banner => { result.bannersMap[banner.bannerType] = banner; });
    return result;
  }
}
