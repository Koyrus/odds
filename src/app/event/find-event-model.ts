export class FindEventModel {
  widgetType: string;
  brokerName: string;
  sportName: string;
  competitionName: string;
  teamName: string;
  participantName: string;
  metaKeywords: string;
  pageUrl: string;
}
