import { Injectable } from '@angular/core';
import { Meta } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import { EventData } from './EventData';
import { BrokerData } from './BrokerData';
import { FindEventModel } from './find-event-model';
import { environment } from '../../environments/environment';

@Injectable()
export class LocatorService {

  private get getBrokerUrl(): string {
    return (this.brokerName != null && this.brokerName.length > 0) ? `${environment.apiHost}/widget/broker/${this.brokerName}` : null;
  }

  private get findEventUrl(): string {
    return `${environment.apiHost}/widget/event`;
  }

  private selectionsUrl(eventId: string): string {
    return `${environment.apiHost}/widget/event/${eventId}/selections`;
  }

  private widgetType: string;
  private brokerName: string;
  private sportName: string;
  private competitionName: string;
  private teamName: string;
  private participantName: string;

  brokerData: BrokerData;
  eventData: EventData;

  constructor(private readonly httpClient: HttpClient, private meta: Meta) { }

  async init(widgetType: string, brokerName: string, sportName: string, competitionName: string, teamName: string, participantName: string) {
    this.widgetType = widgetType;
    this.brokerName = brokerName;
    this.sportName = sportName;
    this.competitionName = competitionName;
    this.teamName = teamName;
    this.participantName = participantName;

    this.brokerData = await this.getBroker();
    this.eventData = await this.getEvent();
  }

  private async getBroker(): Promise<BrokerData> {
    return (this.getBrokerUrl != null) ? await this.httpClient.get<BrokerData>(this.getBrokerUrl).map(data => BrokerData.copy(data)).toPromise() : null;
  }

  private async getEvent(): Promise<EventData> {
    if (this.brokerData == null) {
      return null;
    }

    let keywordsTag = this.meta.getTag('keywords');
    let descriptionTag = this.meta.getTag('description');
    let keywords = (keywordsTag != null ? keywordsTag.content : '') +
      (descriptionTag != null ? descriptionTag.content : '');

    let findEventModel = new FindEventModel();
    findEventModel.widgetType = this.widgetType;
    findEventModel.brokerName = this.brokerData.brokerName;
    findEventModel.sportName = this.sportName;
    findEventModel.competitionName = this.competitionName;
    findEventModel.teamName = this.teamName;
    findEventModel.participantName = this.participantName;
    findEventModel.metaKeywords = keywords;
    findEventModel.pageUrl = window.location.href;

    return await this.httpClient.post<EventData>(this.findEventUrl, findEventModel).toPromise();
  }

  async getSelections(eventId: string): Promise<EventData> {
    return await this.httpClient.get<EventData>(this.selectionsUrl(eventId)).map(data => EventData.copy(data)).toPromise();
  }
}
