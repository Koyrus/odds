export class EventData {
  externalEventId: string;
  sportName: string;
  eventName: string;
  eventTime: string;
  betNowRedirectUrl: string;
  promoBannerTextMessage: string;

  metadata: { [name: string]: string };
  get(metadataKey: string) { return this.metadata[metadataKey] != null ? this.metadata[metadataKey] : ''; }
  markets: MarketSelection[];

  static copy(data: EventData): EventData {
    let result = new EventData();
    result = Object.assign(result, data);
    result.markets = Array.from(data.markets, m =>  MarketSelection.copy(result, m));
    return result;
  }
}

export class MarketSelection {
  name: string;
  marketExternalId: string;
  betNowUrl: string;
  event: EventData;
  selections: SelectionData[];
  static copy(event: EventData, data: MarketSelection): MarketSelection {
    let result = new MarketSelection();
    result = Object.assign(result, data);
    result.event = event;
    result.selections = Array.from(data.selections, s => SelectionData.copy(result, s));
    return result;
  }


}

export class SelectionData {
  name: string;
  currentPriceUp: number;
  currentPriceDown: number;
  betNowUrl: string;
  metadata: { [name: string]: string }
  market: MarketSelection;
  get(metadataKey: string) { return this.metadata[metadataKey] != null ? this.metadata[metadataKey] : "" }

  static copy(market: MarketSelection, data: SelectionData): SelectionData {
    let result = new SelectionData();
    result.market = market;
    result = Object.assign(result, data);
    return result;
  }
}
