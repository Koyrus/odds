import {Component, OnInit, Input, Injectable} from '@angular/core';
import { Banner, BrokerData } from '../event/BrokerData';

@Component({
  selector: 'promobanner',
  templateUrl: './promobanner.component.html',
})
@Injectable()
export class PromobannerComponent implements OnInit {
  public banner: Banner;
  @Input() hasBanner: boolean;
  @Input() broker: BrokerData;

  @Input() bannerType: string;

  @Input() eventPromoText: string;

  constructor() {}
  ngOnInit(): void {
    this.banner = (this.broker != null && this.bannerType != null) ? this.broker.bannersMap[this.bannerType] : null;
    if (this.banner != null) {
      this.hasBanner = true;
    }
  }


}
