import { Component, OnInit, Input } from '@angular/core';
import { SelectionData } from "../event/EventData";

@Component({
  selector: 'selections-panel',
  templateUrl: './selections-panel.component.html',
  styleUrls: ['./selections-panel.component.css']
})
export class SelectionsPanelComponent{
  @Input() participants: SelectionData[];
}
