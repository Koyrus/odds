import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectionsPanelComponent } from './selections-panel.component';

describe('HorseRaceComponent', () => {
  let component: SelectionsPanelComponent;
  let fixture: ComponentFixture<SelectionsPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SelectionsPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectionsPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
