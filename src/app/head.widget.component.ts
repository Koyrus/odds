import { Component, OnInit, ElementRef, ViewEncapsulation } from '@angular/core';
import { LocatorService } from './event/locator.service';
import { BrokerData } from './event/BrokerData';
import { EventData } from './event/EventData';

@Component({
  selector: 'betting-head',
  styleUrls: ['./app.component.scss'],
  templateUrl: './app.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [LocatorService]
})
export class HeadWidgetComponent implements OnInit {
  brokerData: BrokerData;
  eventData: EventData;
  columns = 1;
  secondMarket = '';

  constructor(private elementRef: ElementRef, private eventLocator: LocatorService) { }

  async ngOnInit() {
    let widgetType = this.elementRef.nativeElement.getAttribute('widget_type');
    let brokerName = this.elementRef.nativeElement.getAttribute('broker');
    let sportName = this.elementRef.nativeElement.getAttribute('sport');
    let competitionName = this.elementRef.nativeElement.getAttribute('competition');
    let teamName = this.elementRef.nativeElement.getAttribute('team');
    let participantName = this.elementRef.nativeElement.getAttribute('participant');

    this.columns = +this.elementRef.nativeElement.getAttribute('columns') || this.columns;
    this.secondMarket = this.elementRef.nativeElement.getAttribute('market2');
    const instance = this;
    await setInterval(function () {
        instance.init(widgetType, brokerName, sportName, competitionName, teamName, participantName);
    }, 60000);
    await this.init(widgetType, brokerName, sportName, competitionName, teamName, participantName);
  }

  private async init(widgetType: string, brokerName: string, sportName: string, competitionName: string, teamName: string, participantName: string) {
      console.log('init');
    await this.eventLocator.init(widgetType, brokerName, sportName, competitionName, teamName, participantName);
    this.brokerData = this.eventLocator.brokerData;
    this.eventData = this.eventLocator.eventData;
  }

}
