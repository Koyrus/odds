import {Component, HostListener, Input, OnInit} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LocatorService } from '../event/locator.service';
import { BrokerData } from '../event/BrokerData';
import { EventData, MarketSelection, SelectionData} from '../event/EventData';

@Component({
  selector: 'horse-race-widget',
  templateUrl: './horse-race-widget.component.html',
  styleUrls: ['./horse-race-widget.component.scss'],
  providers: []
})
export class HorseRaceWidgetComponent implements OnInit {
  @Input() broker: BrokerData;
  @Input() columns;
  @Input() secondMarket: string;
  participants1: SelectionData[];
  participants2: SelectionData[];

  public display: Boolean = false;
  public marketDTO;
  public eventNameForColumn;
  @Input() arrayResponseData;
  public pagedItems;
  public screenWidth;
  market2: MarketSelection;

  constructor(private http: HttpClient, private context: LocatorService) {
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.screenWidth = window.innerWidth;
  }

  private static sortCompetitors(p1: SelectionData, p2: SelectionData): number {
    const p1Value = +p1.get('CompetitorNumber');
    const p2Value = +p2.get('CompetitorNumber');
    return p1Value > p2Value ? 1 : (p1Value < p2Value ? -1 : 0);
  }

  private _race: EventData;
  get isHeadToHead(): boolean {
    return this._market.name === 'Head to Head';
  }
  get isCorrectScore(): boolean {
    return this._market.name === 'Correct Score';
  }
  get race(): EventData {
    return this._race;
  }
  set race(value: EventData) {
    if (value != null && value.markets != null && value.markets.length > 0) {
      this.market = (this._market != null)
        ? value.markets.find(v => v.marketExternalId === this._market.marketExternalId) || value.markets[0]
        : value.markets[0];
    } else {
      this.market = null;
    }
    this._race = value;
  }

  private _market: MarketSelection;
  @Input()
  get market(): MarketSelection {
    return this._market;
  }
  set market(value: MarketSelection) {
    this.marketDTO = value;
    this.eventNameForColumn = this.marketDTO.event.eventName;


    if (value != null) {
      let participants = value.selections;
      participants = participants.sort((p1, p2) => HorseRaceWidgetComponent.sortCompetitors(p1, p2));

      this.arrayResponseData = participants;
      this.pagedItems = this.arrayResponseData;
    } else {
      this.participants1 = null;
      this.participants2 = null;
      this.arrayResponseData = null;
    }
    if (value.name === 'Head to Head') {
      this.participants2 = this.participants1.concat([]).slice(0, 1);
      this.participants1 = this.participants1.concat([]).slice(1, 2);
    }

    this._market = value;
  }
  @Input() set eventData(value: EventData)
  {
    if (value != null) {
      this.loadRaces(value.externalEventId).catch((err) => alert(`Unexpected error! ${err.message}`));
    }
  }

  private async loadRaces(eventId: string) {
    const race = await this.context.getSelections(eventId);
    if (this.secondMarket != null && this.secondMarket.length > 0) {
      this.market2 = race.markets.find(v => v.name === this.secondMarket);
      if (this.market2 != null) {
        let participants = this.market2.selections;
        this.participants2 = participants.slice(0, 6);
        this.columns = 2;
        const index = race.markets.indexOf(this.market2, 0);
        if (index > -1) {
          // race.markets.splice(index, 1); // Keep second market for now...
        }
      } else {
        this.secondMarket = null;
        this.columns = 1;
      }
    }

    this.race = race;
  }

  setMarket(market) {
    this.market = market;
  }

  ngOnInit(): void {
    this.screenWidth = window.innerWidth;
  }
}

