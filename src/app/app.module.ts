import { NgModule, ComponentFactoryResolver, ApplicationRef, Injector } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { FormsModule } from '@angular/forms';
import { BrowserModule, Meta } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { ResponsiveModule } from 'ngx-responsive';
import { AppComponent } from './app.component';
import { ResponsiveWrapperComponent } from './responsive-wrapper/responsive-wrapper.component';
import { ParticipantsListComponent } from './participants-list/participants-list.component';
import { HorseRaceWidgetComponent } from './horse-race-widget/horse-race-widget.component';
import { SelectionsPanelComponent } from './selections-panel/selections-panel.component';
import { ParticipantComponent } from './participant/participant.component';
import { HeadlineComponent } from './headline/headline.component';
import { HorseraceParticipantComponent } from './participant/horserace-participant/horserace-participant.component';
import { FootballParticipantComponent } from './participant/football-participant/football-participant.component';
import { EmptyWidgetComponent } from './empty-widget/empty-widget.component';
import { LinkbannerComponent } from './linkbanner/linkbanner.component';
import { PromobannerComponent } from './promobanner/promobanner.component';
import { BannerComponent, BannersListComponent } from './banners';
import { OtherMarketsComponent } from './other-markets/other-markets.component';
import {CarouselModule} from 'primeng/carousel';
import {Angular2FontawesomeModule} from 'angular2-fontawesome';

@NgModule({
  declarations: [
    AppComponent,
    ResponsiveWrapperComponent,
    ParticipantsListComponent,
    HeadlineComponent,
    HorseRaceWidgetComponent,
    SelectionsPanelComponent,
    ParticipantComponent,
    HorseraceParticipantComponent,
    FootballParticipantComponent,
    EmptyWidgetComponent,
    LinkbannerComponent,
    PromobannerComponent,
    BannerComponent,
    BannersListComponent,
    OtherMarketsComponent,
  ],
  entryComponents: [HorseRaceWidgetComponent, EmptyWidgetComponent, AppComponent ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ResponsiveModule,
    BrowserAnimationsModule,
    CarouselModule,
    Angular2FontawesomeModule
  ],
  providers: [Meta],
  // bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private injector: Injector, private cfr: ComponentFactoryResolver, private appRef: ApplicationRef) {
  }
  ngDoBootstrap(): void {
    const widgetCompFactory = this.cfr.resolveComponentFactory(AppComponent);

    let els = document.querySelectorAll(widgetCompFactory.selector), i;

    for (i = 0; i < els.length; ++i) {
      let el = els[i];
      this.appRef.bootstrap(widgetCompFactory, el);
    }

  }
}
