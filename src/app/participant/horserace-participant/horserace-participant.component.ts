import {Component, Injectable, Input} from '@angular/core';
import { SelectionData } from "../../event/EventData";

@Component({
  selector: 'horserace-participant',
  templateUrl: './horserace-participant.component.html',
  styleUrls: ['../participant.component.scss']
})
@Injectable()
export class HorseraceParticipantComponent {
  @Input() participant: SelectionData;
  @Input() odds: string;
  public id;
}
