import { Component, Input } from '@angular/core';
import { SelectionData } from '../event/EventData';

@Component({
  selector: 'participant',
  styleUrls: ['./participant.component.scss'],
  template: `
  <football-participant *ngIf="participant.market.event.sportName==='FOOTBALL'" [participant]="participant" [odds]="odds"></football-participant>
  <horserace-participant *ngIf="participant.market.event.sportName==='HORSERACING'" [participant]="participant" [odds]="odds"></horserace-participant>
`,
})
export class ParticipantComponent{
  odds: string;

  private _participant: SelectionData;
  @Input()
  get participant(): SelectionData {
    return this._participant;
  }
  set participant(value: SelectionData) {
    this._participant = value;
  }
}
