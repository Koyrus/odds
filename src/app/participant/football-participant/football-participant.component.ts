import {AfterViewInit, Component, Injectable, Input, OnChanges, OnInit} from '@angular/core';
import {EventData, SelectionData} from '../../event/EventData';
import {HorseRaceWidgetComponent} from '../../horse-race-widget/horse-race-widget.component';

@Component({
  selector: 'football-participant',
  templateUrl: './football-participant.component.html',
  styleUrls: ['../participant.component.scss']
})
@Injectable()
export class FootballParticipantComponent {
  @Input() participant: SelectionData;
  @Input() odds: string;

}
