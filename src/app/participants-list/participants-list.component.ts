import {Component, HostListener, Input, OnInit} from '@angular/core';
import {BrokerData} from '../event/BrokerData';
import {EventData} from '../event/EventData';
import {HorseRaceWidgetComponent} from '../horse-race-widget/horse-race-widget.component';
import {ParticipantsService} from './participants.service';

@Component({
  selector: 'participants-list',
  templateUrl: './participants-list.component.html',
  styleUrls: ['./participants-list.component.scss'],
  providers: []
})
export class ParticipantsListComponent implements OnInit {
  @Input() headToHead: boolean;
  @Input() CorrectScore: boolean;
  @Input() HalfTime: boolean;
  @Input() hasBanner: boolean;
  @Input() columns: number;
  @Input() event: EventData;
  @Input() broker: BrokerData;
  public selected;
  public screenWidth;
  public eventName;
  public selectedValue: any = '';
  public upTo;
  public downTo;
  public redirectLink;
  pager: any = {};
  pagedItems: any[];
  constructor(public horse: HorseRaceWidgetComponent) {
    this.pagedItems = this.horse.arrayResponseData.slice(this.pager.startIndex, this.pager.endIndex + 1);
    if (this.horse.marketDTO.name === 'Correct Score') {
      this.eventName = this.horse.marketDTO.event.eventName;
    }
  }
  changeSelectionsName() {
    this.downTo = this.selectedValue.currentPriceDown;
    this.upTo = this.selectedValue.currentPriceUp;
    this.redirectLink = this.selectedValue.betNowUrl;
  }
  @HostListener('document:click')
  onChangePageEventEmiter() {
      let item = event['path']['0'].className;
      if (item === 'dropdownItem') {
        this.setPage(1);
      }
  }
  @HostListener('window:resize', ['$event'])
  onResize() {
    this.screenWidth = window.innerWidth;
  }

  ngOnInit(): void {
    this.setPage(1);
    this.screenWidth = window.innerWidth;
  }
  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }
    this.pager = ParticipantsService.getPager(this.horse.arrayResponseData.length, page);
  }
}

