import {Component, Input, Output, EventEmitter, OnInit, HostListener} from '@angular/core';
import { EventData, MarketSelection } from '../event/EventData';
import { BrokerData} from '../event/BrokerData';
import {ParticipantsService} from '../participants-list/participants.service';
@Component({
  selector: 'headline',
  styleUrls: ['./headline.component.scss'],
  templateUrl: './headline.component.html',
})
export class HeadlineComponent implements OnInit {
  @Input() event: EventData;
  @Input() broker: BrokerData;
  @Input() headToHead: boolean;
  public screenWidth;
  private _market: MarketSelection;
  public idForToogler: any;
  @Input()
  get market(): MarketSelection {
    ParticipantsService.getPager(1);
    return this._market;
  }
  set market(value: MarketSelection) {
    if (this._market !== value) {
      this._market = value;
      this.onMarketChanged.emit(value);
      ParticipantsService.getPager(1);
    }
  }
  public active: Boolean = false;
  @Output() onMarketChanged = new EventEmitter<MarketSelection>();
  setMarket(market) {
    this.market = market;
    if (market != null) {
      this.active = false;
    }
  }
  toggleVisibility() {
    this.active = !this.active;
  }
  @HostListener('window:resize', ['$event'])
  onResize() {
    this.screenWidth = window.innerWidth;
  }
  ngOnInit(): void {
    this.idForToogler = this.market.event['id'];
    this.screenWidth = window.innerWidth;
  }
}
