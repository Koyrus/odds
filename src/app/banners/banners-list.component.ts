import {Component} from '@angular/core';

@Component({
  selector: 'banners-list',
  styleUrls: ['./banners-list.component.scss'],
  template: '<ng-content></ng-content>',
})
export class BannersListComponent {
}
