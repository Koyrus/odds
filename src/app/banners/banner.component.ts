import {Component, Input} from '@angular/core';

@Component({
  selector: 'banner',
  styleUrls: ['./banner.component.scss'],
  templateUrl: './banner.component.html',
})
export class BannerComponent {
  @Input() className: string = '';
  @Input() href: string;
  @Input() title: string;
  @Input() image: string;
}
