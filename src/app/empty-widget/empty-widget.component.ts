import { Input, Component } from '@angular/core';
import { BrokerData } from '../event/BrokerData';

@Component({
  selector: 'missing-event-widget',
  templateUrl: './empty-widget.component.html'
})
export class EmptyWidgetComponent  {
  @Input() broker: BrokerData;
}
