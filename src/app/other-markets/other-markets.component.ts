import {Component, Input, Output, EventEmitter} from '@angular/core';
import { MarketSelection } from '../event/EventData';

@Component({
  selector: 'other-markets',
  styleUrls: ['./other-markets.component.scss'],
  templateUrl: './other-markets.component.html',
})
export class OtherMarketsComponent {
  @Input() markets: MarketSelection[];
  @Output() onSelect = new EventEmitter<MarketSelection>();
  public active: Boolean = false;
  toggleVisibility() {
    this.active = !this.active;
  }
}
